module ReservationsHelper
	def amount_for_stripe(price_total)
		# Stripe requires the amount in centimes
		return price_total * 100
	end
end
