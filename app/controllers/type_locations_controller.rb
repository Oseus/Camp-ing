require 'json'

class TypeLocationsController < ApplicationController
  before_action :set_type_location, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:new, :create, :edit, :update, :destroy]

  # GET /type_locations
  # GET /type_locations.json
  def index
    @type_locations = TypeLocation.all
  end

  # GET /type_locations/1
  # GET /type_locations/1.json
  def show
    @@type_location = @type_location
  end

  def date_part
    @reception_begin = params[:data_infos];
    puts @reception_begin 
  end

  def date_part_2
    # Data from params
    puts params
    tl = TypeLocation.find_by_id(params.permit(:id_type_location).values.first)
    proposition_end = Date.parse(params.permit(:date_end).values.first)
    proposition_begin = Date.parse(params.permit(:date_begin).values.first)
    
    # useful var
    id_first_free_loc = nil
    
    tl.locations.each do |l|
      free_loc = true
      l.reservations.each do |reserv| 
        if ( ( (reserv.date_begin >= proposition_begin) && (reserv.date_begin < proposition_end) ) || ( (reserv.date_end > proposition_begin) && (reserv.date_end <= proposition_end) || (( reserv.date_begin < proposition_begin) && (reserv.date_end) > proposition_end ) )) 
          puts "Dans le if"
          # Une réservation est dans le créneau choisie, cette location n'est pas disponible
          free_loc = false
        end
      end
      puts "Fin des reservation"
      puts free_loc
      if free_loc
        # After all each of resservation, the location is free. We keep the first if of free locations
        id_first_free_loc ||= l.id
        redirect_to new_reservation_path(date_begin: params.permit(:date_begin).values.first, date_end: params.permit(:date_end).values.first, location_id: id_first_free_loc)
      end
    end

    if id_first_free_loc == nil  
      puts "Normalement, il y a alert"
      flash.now[:alert] = "Les dates ne sont pas disponibles"
    end
  end

  # GET /type_locations/new
  def new
    @type_location = TypeLocation.new
  end

  # GET /type_locations/1/edit
  def edit
  end

  # POST /type_locations
  # POST /type_locations.json
  def create
    @type_location = TypeLocation.new(type_location_params)

    update_locations_association
    update_category_association

    respond_to do |format|
      if @type_location.save
        format.html { redirect_to @type_location, notice: 'Type location was successfully created.' }
        format.json { render :show, status: :created, location: @type_location }
      else
        format.html { render :new }
        format.json { render json: @type_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /type_locations/1
  # PATCH/PUT /type_locations/1.json
  def update
    update_locations_association
    update_category_association
    respond_to do |format|
      if @type_location.update(type_location_params)
        format.html { redirect_to @type_location, notice: 'Type location was successfully updated.' }
        format.json { render :show, status: :ok, location: @type_location }
      else
        format.html { render :edit }
        format.json { render json: @type_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /type_locations/1
  # DELETE /type_locations/1.json
  def destroy
    @type_location.destroy
    respond_to do |format|
      format.html { redirect_to type_locations_url, notice: 'Type location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_location
      @type_location = TypeLocation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def type_location_params
      hash_params = params.fetch(:type_location, {})
      if params.has_key?(:category)
        hash_params.merge(params.permit(:category))
        hash_params.permit :title, :description, :nb_person, :price_per_night, :category
      end
    end

    def update_locations_association 
      nb_form = params.require(:type_location).permit(:nb_emplacement).values.first.to_i
      nb_object = @type_location.locations.size
      if (nb_form > nb_object)
        (nb_form-nb_object).times do |i|
          @type_location.locations.push( Location.create(id_in_camping: (type_location_params[:title] + "_" + (i+1).to_s) ) )
        end
      elsif (nb_form == nb_object)
      else 
        (nb_object-nb_form).times do |i|
          @type_location.locations.last.delete
        end
      end
    end

    def update_category_association
      categ = Category.find_by_id(params.permit(:category).values.first.to_i)
      @type_location.category = categ if @type_location.category != categ
    end

end
