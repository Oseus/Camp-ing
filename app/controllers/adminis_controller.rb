class AdminisController < ApplicationController
before_action :authenticate_admin!

def home
end

	def user_list
	    @user_list = Client.all
	end


  def destroy
    @user_list = Client.find_by(params[:id])
    @user_list.destroy
    redirect_to user_list_path
    respond_to do |format|
      format.html { redirect_to user_list_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def reservation
    @reservation = Reservation.all
  end

  def location
    @location = Location.all
  end

  def new_location
    @location = Location.new
  end
def create_camping
    @location = Location.new(location_params)

    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: 'Location was successfully added.' }
        format.json { render :show, status: :created, location: @location }
      else
        format.html { render :new }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  def camping
    @camping = Camping.all
  end

  def new_camping
    @camping = Camping.new
  end
def create_camping
    @camping = Camping.new(camping_params)

    respond_to do |format|
      if @camping.save
        format.html { redirect_to @camping, notice: 'Camping was successfully added.' }
        format.json { render :show, status: :created, location: @camping }
      else
        format.html { render :new }
        format.json { render json: @camping.errors, status: :unprocessable_entity }
      end
    end
  end

  def category
    @category = Category.all
  end

  private

  def camping_params
    params.require(:camping).permit(:name, :description, :number, :adress)
  end

  def location_params
    params.require(:location).permit(:price_per_night)
  end

end
