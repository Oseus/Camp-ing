class CampingsController < ActionController::Base
	before_action :create_if_necessary
  	before_action :authenticate_admin! 


	def show
	end

	def edit
	end

	def update
		respond_to do |format|
	      if @camping.update(params.require(:camping).permit(:name, :description, :number, :schedule_dayly, :adress))
	        format.html { redirect_to campinge_path, notice: 'camping was successfully updated.' }
	        format.json { render :show, status: :ok, location: @camping }
	      else
	        format.html { render :edit }
	        format.json { render json: @camping.errors, status: :unprocessable_entity }
	      end
	    end
	end

	protected 
	def create_if_necessary
		@camping = Camping.all.size > 0 ? Camping.last : Camping.create 
	end
end
