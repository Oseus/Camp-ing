class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:edit, :update, :destroy]
  before_action :authenticate_client!, only: [:create, :new]

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = current_client.reservations.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new(reservation_params)
    @reservation.client = current_client
    @reservation.location = Location.find_by_id(params.permit(:location_id).values.first.to_i)

    # Price_per_night of location is prioritized 
    @price_p_n = @reservation.location.price_per_night || @reservation.location.type_location.price_per_night
    
    @reservation.date_begin = Date.parse(params.permit(:date_begin).values.first)+1
    @reservation.date_end = Date.parse(params.permit(:date_end).values.first)+1

    @reservation.price = (@reservation.date_end - @reservation.date_begin) * @price_p_n
    @reservation.save
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    r = Reservation.last
    # r.completed = true
    # r.save
    redirect_to reservations_path(r)
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to reservations_url, notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.fetch(:reservation, {})
    end
end
