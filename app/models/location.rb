class Location < ApplicationRecord
	has_many :reservations, dependent: :destroy
   	belongs_to :type_location
   	validates :id_in_camping, presence: true

end
