class Reservation < ApplicationRecord
	belongs_to :location
	belongs_to :client

	# Validate when title, price, nb_person and location are present
   	validates :date_begin, :date_end, :price, :client, :location, presence: true

end
