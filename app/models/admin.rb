class Admin < ApplicationRecord
  	# Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

   	# Validate when firstname and name are present
   	validates :firstname, presence: true
    validates :name, presence: true
end
