class Client < ApplicationRecord
  	 # Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
   	has_many :reservations
   	
   	# Validate when firstname and name are present
   	validates :firstname, :name, presence: true

end
