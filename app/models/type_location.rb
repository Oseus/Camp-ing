class TypeLocation < ApplicationRecord
  belongs_to :category, optional: true
  has_many :locations
  validates :title, :price_per_night, :nb_person, presence: true
end
