class Category < ApplicationRecord
	has_many :type_locations
	# Validate whe	n name is present
   	validates :name, presence: true
end
