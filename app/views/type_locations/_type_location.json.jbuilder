json.extract! type_location, :id, :created_at, :updated_at
json.url type_location_url(type_location, format: :json)
