json.extract! camping, :id, :created_at, :updated_at
json.url camping_url(camping, format: :json)