Rails.application.routes.draw do
  root to: 'home#Index'

  post "/date_part" => "type_locations#date_part", :as => :typelocation_date_part
  post "/date_part_2" => "type_locations#date_part_2", :as => :typelocation_date_part_2


  get 'adminis/user_list', to: 'adminis#user_list', as: 'user_list'
  get 'adminis/reservation', to: 'adminis#reservation', as: 'reservation'
  get 'adminis/location', to: 'adminis#location', as: 'location'
  get 'adminis/location/new', to: 'adminis#new_location', as: 'new_location'
  post 'adminis/location/new', to: 'adminis#create_location', as: 'create_location'


  get 'adminis/category', to: 'adminis#category', as: 'category'
  get 'adminis/camping', to: 'adminis#camping', as: 'campinge'
  get 'adminis/camping/new', to: 'adminis#new_camping', as: 'new_camping'

  post 'adminis/camping/new', to: 'adminis#create_camping', as: 'create_camping'

  # Resource without 's' to not use id of camping in the url
  resource :camping, only:[:show,:edit, :update], path: 'admin/camping'
  resources :type_locations
  resources :reservations


  devise_for :admins, path: 'admins'
  devise_for :clients, path: 'clients'

  resources :adminis do
    member do
      get 'user_list'
      get 'reservation'
      get 'location'
      get 'category'
      get 'camping'
    end

end
end
