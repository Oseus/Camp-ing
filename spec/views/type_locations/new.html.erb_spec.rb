require 'rails_helper'

RSpec.describe "type_locations/new", type: :view do
  before(:each) do
    assign(:type_location, TypeLocation.new())
  end

  it "renders new type_location form" do
    render

    assert_select "form[action=?][method=?]", type_locations_path, "post" do
    end
  end
end
