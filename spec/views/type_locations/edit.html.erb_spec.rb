require 'rails_helper'

RSpec.describe "type_locations/edit", type: :view do
  before(:each) do
    @type_location = assign(:type_location, TypeLocation.create!())
  end

  it "renders the edit type_location form" do
    render

    assert_select "form[action=?][method=?]", type_location_path(@type_location), "post" do
    end
  end
end
