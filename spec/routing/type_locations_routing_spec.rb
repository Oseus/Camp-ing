require "rails_helper"

RSpec.describe TypeLocationsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/type_locations").to route_to("type_locations#index")
    end

    it "routes to #new" do
      expect(:get => "/type_locations/new").to route_to("type_locations#new")
    end

    it "routes to #show" do
      expect(:get => "/type_locations/1").to route_to("type_locations#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/type_locations/1/edit").to route_to("type_locations#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/type_locations").to route_to("type_locations#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/type_locations/1").to route_to("type_locations#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/type_locations/1").to route_to("type_locations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/type_locations/1").to route_to("type_locations#destroy", :id => "1")
    end

  end
end
