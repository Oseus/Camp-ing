require 'rails_helper'

RSpec.describe Location, type: :model do
  it "is valid with valid attributes" do
    expect(build(:location)).to be_valid
  end

  it "is invalid without attribute" do
    expect(Location.new()).to be_invalid
  end

  it "is invalid without a needed attribute" do
    expect(build(:location, id_in_camping: nil)).to be_invalid
  end

  it "has one reservation" do
    should have_many(:reservations)
  end  

  it "belongs to one type_location" do
    should belong_to(:type_location)
  end  

  it "is valid with valid attributes" do
    expect(build(:location_with_reservations)).to be_valid
  end
end
