require 'rails_helper'

RSpec.describe TypeLocation, type: :model do
  it "is valid with valid attributes" do
    expect(build(:type_location)).to be_valid
  end

  it "is invalid without attribute" do
    expect(TypeLocation.new()).to be_invalid
  end

  it "is invalid without a needed attribute" do
    expect(build(:type_location, nb_person: nil)).to be_invalid
  end

  it "is invalid without a other needed attribute" do
    expect(build(:type_location, title: nil)).to be_invalid
  end

  it "has one reservation" do
    should have_many(:locations)
  end  

  it "has and belongs to many categories" do
    should belong_to(:category)
  end
end
