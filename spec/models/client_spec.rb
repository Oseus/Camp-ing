require 'rails_helper'

RSpec.describe Client, type: :model do
  it "is valid with valid attributes and few reservations" do
    expect(build(:client_with_reservations)).to be_valid
  end

  it "is valid with valid attributes and no reservation" do
    expect(build(:client)).to be_valid
  end

  it "is invalid without attribute" do
  	expect(Client.new()).to_not be_valid
  end

  it "is invalid without one needed attribute" do
  	expect(build(:client, name: nil)).to_not be_valid
  end
end
