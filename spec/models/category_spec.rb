require 'rails_helper'

RSpec.describe Category, type: :model do
  it "is valid with valid attributes" do
    expect(Category.new(name:'Chalet', description:"Un emplacement avec un toit !")).to be_valid
  end

  it "is valid with only need attributes" do
    expect(Category.new(name:'Chalet')).to be_valid
  end

  it "is not valid without a name" do
  	expect(Category.new(description:'Un emplacement avec un toit !')).to be_invalid
  end

end
