require 'rails_helper'

RSpec.describe Reservation, type: :model do
  
  it "is valid with valid attributes" do
  	expect( build(:reservation)).to be_valid
  end

  it "has one client" do
    should belong_to(:client)
  end  

  it "has one location" do
    should belong_to(:location)
  end

  it "is invalid with no attribute" do
    expect(Reservation.new()).to_not be_valid
  end

  it "is invalid with no assocation attribute" do
    expect(build(:reservation, client: nil, location: nil)).to_not be_valid
  end

  it "is invalid without a assocation attribute" do
    expect(build(:reservation, location: nil)).to_not be_valid
  end

end
