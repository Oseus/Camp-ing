require 'rails_helper'

RSpec.describe Admin, type: :model do
  it "is valid with valid attributes" do
    expect(build(:admin)).to be_valid
  end

  it "is invalid without attribute" do
  	expect(Admin.new()).to_not be_valid
  end

  it "is invalid without one needed attribute" do
  	expect(build(:admin, name: nil)).to_not be_valid
  end
end
