FactoryBot.define do
  factory :camping do
    name "MyString"
    description "MyText"
    number "MyString"
    schedule_dayly "MyString"
    adress "MyString"
  end
end
