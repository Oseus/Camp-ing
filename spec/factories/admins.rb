FactoryBot.define do
  factory :admin do
	firstname "testFirstName" 
    name "testName"
    email "aaa@bbb.ccc"
    password "azerty"
    password_confirmation "azerty"    
  end
end
