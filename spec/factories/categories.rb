FactoryBot.define do
  factory :category do
    name "Chalet"
    description "A location with roof"
  end
end