FactoryBot.define do
  factory :reservation do
    date_begin Time.now
    date_end Time.now+3.week
    price 1.5
    completed false
    association :client, factory: :client
    association :location, factory: :location
  end
end
