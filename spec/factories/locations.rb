FactoryBot.define do
  factory :location do
    price_per_night 6.2
    id_in_camping Random.rand(30)
    association :type_location, factory: :type_location

    factory :location_with_reservations do
      transient do
        reservations_count 5
      end
      after(:create) do |location, evaluator|
        create_list(:reservation, evaluator.resservations_count, location: location)
      end
    end   
  end
  
end
