FactoryBot.define do
  factory :type_location do
    price_per_night 1.5
    title "MyString"
    description "MyText"
    nb_person Random.rand(8)

    factory :type_location_with_category do
      association :category, factory: :category
    end
  end
end
