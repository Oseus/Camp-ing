FactoryBot.define do
  factory :client do 
    firstname "testFirstName" 
    name "testName"
    email "aaa@bbb.ccc"
    password "azerty"
    password_confirmation "azerty"

    factory :client_with_reservations do
      transient do
        reservations_count 5
      end
      after(:create) do |client, evaluator|
        create_list(:reservation, evaluator.resservations_count, client: client)
      end
    end
  end
end
