# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

nb_location = 15
nb_client = 10
cat = ["Chalet", "Bungalow", "Espace tente", "Tipi", "Cabane", "Espace camping-car"]


cat.length.times do |i|
	Category.create( name: cat[i], description: Faker::Lorem.sentence(3))
end

nb_location.times do 
	l = TypeLocation.new(title: Faker::BreakingBad.episode, 
		description: Faker::Lorem.sentence(3), 
		price_per_night: Faker::Number.decimal(2, 2), 
		nb_person: Faker::Number.normal(5, 2),  
		nb_location: Faker::Number.normal(10, 4)
	)
	l.category = Category.all.sample
	l.save
end

10.times do |i|
	Client.create( firstname: Faker::Name.first_name, 
		name: Faker::Name.last_name,
		email: "client"+i.to_s+"@aaa.bb",
    	password: "azerty",
    	password_confirmation: "azerty", 
    	number: Faker::PhoneNumber.phone_number
    )
end

15.times do |i|
	res = Reservation.new
	res.date_begin = Time.now+rand(3).day
	res.date_end = Time.now+4.day+rand(21).day
	res.client = Client.all.sample
	res.location = Location.all.sample
	res.price = res.location.price_per_night * (res.date_end - res.date_begin).to_i
	res.save
end

    # date_begin Time.now
    # date_end Time.now+3.week
    # price 1.5
    # completed false
    # association :client, factory: :client
    # association :location, factory: :location