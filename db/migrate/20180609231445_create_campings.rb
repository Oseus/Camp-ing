class CreateCampings < ActiveRecord::Migration[5.2]
  def change
    create_table :campings do |t|
      t.string :name
      t.text :description
      t.string :number
      t.string :schedule_dayly
      t.string :adress

      t.timestamps
    end
  end
end
