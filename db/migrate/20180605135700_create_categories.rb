class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
    create_table :categories_locations, id: false do |t|
      t.belongs_to :category, index: true
      t.belongs_to :location, index: true
    end
  end
end
