class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.date :date_begin
      t.date :date_end
      t.float :price
      t.boolean :completed

      t.timestamps
    end
  end
end
