class AddAssociations < ActiveRecord::Migration[5.2]
  def change
  	# Association reservation have a location reference 
  	add_reference :reservations, :location, foreign_key: true
  	# Association reservation have a client reference
  	add_reference :reservations, :client, foreign_key: true
  end
end