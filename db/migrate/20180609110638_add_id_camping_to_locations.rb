class AddIdCampingToLocations < ActiveRecord::Migration[5.2]
  def change
    add_column :locations, :id_in_camping, :string
  end
end
