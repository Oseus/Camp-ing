class CreateTypeLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :type_locations do |t|
      t.float :price_per_night
      t.string :title
      t.text :description
      t.integer :nb_person
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
