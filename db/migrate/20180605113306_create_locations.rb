class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.text :description
      t.string :title
      t.integer :nb_location
      t.integer :nb_person
      t.float :price_per_night
      
      t.timestamps
    end
  end
end
