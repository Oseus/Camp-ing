class DeleteDetailsFromLocation < ActiveRecord::Migration[5.2]
  def change
  	remove_column :locations, :title, :string
  	remove_column :locations, :nb_location, :integer
  	remove_column :locations, :nb_person, :integer
  	remove_column :locations, :description, :text
  end
end