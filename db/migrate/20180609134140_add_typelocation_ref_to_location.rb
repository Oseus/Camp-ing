class AddTypelocationRefToLocation < ActiveRecord::Migration[5.2]
  def change
    add_reference :locations, :type_location, foreign_key: true
  end
end
